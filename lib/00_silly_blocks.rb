def reverser(&prc)
  words = prc.call.split
  words.map {|word| word.reverse }.join(" ")
end

def adder(num = 1, &prc)
  prc.call + num
end

def repeater(times = 1, &prc)
  times.times { prc.call }
end
